import glob
import numpy as np
import os
import re
import subprocess
import sys

FIXED_POINT    = 11
INPUT_DIR      = '../dataset/ppm/'
OUTPUT_DIR     = './out'
INPUT_TENSOR   = 'image'
OUTPUT_TENSORS = ['conv6_conv6_1_batchnorm_fusedbatchnorm_scale_memory']

WORK_DIR = os.getcwd()
print 'Your working directory :: %s' % (WORK_DIR)

input_img_list = glob.glob(os.path.join(INPUT_DIR, '*.ppm'))

test_idx = 0 

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)


for img in input_img_list:
    if test_idx == 1: break
    ##############################################################################################
    # STEP1 :: run application for CNNIP 
    #./cnnip_target_project_HIL_GNU_linux --commandlist cl_cnn0.bin --core CNN --sync-id 1 \
    #                                     --commandlist cl_dma0.bin --core DMA --sync-id 2 \
    #                                     --bcl commandlist.bcl \
    #                                     --setMemory image_memory:0, image.ppm[_input_image],0 \
    #                                     --setMemory image_memory:1, image.ppm,1 \
    #                                     --setMemory image_memory:2, image.ppm,2
    ##############################################################################################
    _input_memory_0 = '%s,%s,%d' % (INPUT_TENSOR + '_memory:0', img, 0)
    _input_memory_1 = '%s,%s,%d' % (INPUT_TENSOR + '_memory:1', img, 1)
    _input_memory_2 = '%s,%s,%d' % (INPUT_TENSOR + '_memory:2', img, 2)
    _output_name    = os.path.basename(os.path.join(img)).split('.ppm')[0]
    
    p = subprocess.Popen(['../cnnip_target_project_HIL_GNU_linux', '--commandlist', '../cl_cnn0.bin', '--core', 'CNN', '--sync-id', '1', '--commandlist', '../cl_dma0.bin', '--core', 'DMA', '--sync-id', '2', '--bcl', '../commandlist.bcl', '--setMemory', _input_memory_0, '--setMemory', _input_memory_1, '--setMemory', _input_memory_2])
    p.communicate(input='\n')

    ##############################################################################################
    # STEP2 :: convert output files to npy file and save the npy file
    ##############################################################################################
    for tensor in OUTPUT_TENSORS:
        output_files = [f for f in glob.glob(os.path.join(WORK_DIR, '*.txt')) if tensor in f and '(' in f]
     
        def atoi(text):
            return int(text) if text.isdigit() else text

        def numeric_key(text):
            return [atoi(c) for c in re.split('(\d+)', text)]
        output_files.sort(key=numeric_key)       
        
        txt_list = []
        for txt in output_files:
            print(txt)
            load_image = np.loadtxt(txt)
            load_image = np.divide(load_image, pow(2, FIXED_POINT))
            txt_list.append(load_image.astype(np.float32))
        
        out = np.stack(txt_list)
        out = np.transpose(out, (1, 2, 0))
        
        save_filename = '%s.npy' % (_output_name) 
        np.save(os.path.join(OUTPUT_DIR, save_filename), out)




