import tensorflow as tf
import tensorflow.contrib.slim as slim
import tensorflow.contrib.layers as layers

from tensorpack import *


class Model(ModelDesc):
    def __init__(self):
        self.num_classes=5

    def inputs(self):
        return [tf.placeholder(tf.float32, [None, None, None, 3], 'image'),
                tf.placeholder(tf.int32, [None, None, None, self.num_classes], 'segmentation')]

    def build_graph(self, image, segmentation):
        image_ = tf.identity(image, name='input')
        image = tf.image.convert_image_dtype(image, tf.float32, saturate=True)
        activation_fn = tf.nn.relu
        with slim.arg_scope([slim.conv2d], 
                            activation_fn=activation_fn):
            net = slim.repeat(image, 2, slim.conv2d, 32, [5, 5], scope='conv1')
            net = slim.max_pool2d(net, [2, 2], scope='pool1')
            net = slim.repeat(net, 2, slim.conv2d, 16, [5, 5], scope='conv2')
            net = slim.max_pool2d(net, [2, 2], scope='pool2')
            net = slim.repeat(net, 2, slim.conv2d, 32, [5, 5], scope='conv3')
            net = slim.max_pool2d(net, [2, 2], scope='pool3')
            net = slim.repeat(net, 1, slim.conv2d, 16, [5, 5], scope='conv4')
            net = slim.max_pool2d(net, [2, 2], scope='pool4')
            net = slim.repeat(net, 1, slim.conv2d, 32, [5, 5], scope='conv5')
            net = slim.max_pool2d(net, [2, 2], scope='pool5')
 
        trunc_normal = lambda stddev: tf.truncated_normal_initializer(0.0, stddev)
        weight_decay = 0.00004
        batch_norm_decay = 0.9997
        batch_norm_epsilon = 0.001
        activation_fn = None

        batch_norm_params = {
            'decay': batch_norm_decay, 
            'epsilon': batch_norm_epsilon, 
            'updates_collections': tf.GraphKeys.UPDATE_OPS, 
            'fused': None
        }

        with slim.arg_scope([layers.batch_norm, layers.dropout], 
                            is_training=get_current_tower_context().is_training):
            
            normalizer_fn = slim.batch_norm
            normalizer_params = batch_norm_params
            with slim.arg_scope([slim.conv2d_transpose], 
                weights_regularizer=slim.l2_regularizer(weight_decay)):
                with slim.arg_scope([slim.conv2d_transpose], 
                    weights_initializer=slim.variance_scaling_initializer(),
                    activation_fn=activation_fn,
                    normalizer_fn=normalizer_fn, 
                    normalizer_params=normalizer_params):
                    with slim.arg_scope([slim.conv2d_transpose], 
                        weights_initializer=trunc_normal(0.01), 
                        kernel_size=[5, 5], 
                        stride=2, padding='VALID'):

                        net = slim.convolution2d_transpose(net, num_outputs=64, scope='deconv1_1', 
                                                           activation_fn=None, 
                                                           normalizer_fn=None, 
                                                           normalizer_params={})
                        net = slim.convolution2d_transpose(net, num_outputs=32, scope='deconv2_1', 
                                                           activation_fn=None, 
                                                           normalizer_fn=None, 
                                                           normalizer_params={})
                        net = slim.convolution2d_transpose(net, num_outputs=16, scope='deconv3_1', 
                                                           activation_fn=None, 
                                                           normalizer_fn=None, 
                                                           normalizer_params={})
                        net = slim.convolution2d_transpose(net, num_outputs=8, scope='deconv4_1', 
                                                           activation_fn=None, 
                                                           #normalizer_fn=None, 
                                                           normalizer_params={})
                        net = slim.convolution2d_transpose(net, num_outputs=self.num_classes, scope='upscore', 
                                                           activation_fn=None, 
                                                           normalizer_fn=None, 
                                                           normalizer_params={})
                        #net_seg = net
                        net_seg = tf.image.resize_image_with_crop_or_pad(net, 288, 512)

        logits = net_seg
        assert len(segmentation.shape) == 4
        pred = tf.cast(tf.argmax(net_seg, axis=3), tf.int32, name='prediction')


model = Model()
