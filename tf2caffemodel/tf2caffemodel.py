#!/usr/bin/env python
# -*- coding: utf-8 -*-                                                                                         
# Author: Yeonhwa Cha  

import argparse
import caffe
import caffe.proto.caffe_pb2 as caffe_pb2
import cv2
import google.protobuf as pb
import numpy as np
import os
import subprocess
import tensorflow as tf
import time

from caffe import layers as L
from tensorflow.python.platform import gfile
from tensorflow.python.framework import tensor_util
from tensorpack import *
from tensorpack.tfutils.export import ModelExporter

##########################################################
# PhantomAI Package
import utils

from config.tool import config as cfg
from network.backbone_v1 import model


##########################################################
# [tensorpack] predict
def predict(model_path, pb_path, cfg):
    print("........INPUT_TENSOR [{}], OUTPUT_TENSOR [{}]".format(cfg.INPUT_TENSOR, cfg.OUTPUT_TENSOR))
    pred_config = PredictConfig(
        model=model, 
        session_init=get_model_loader(model_path),
        input_names=[cfg.INPUT_TENSOR], 
        output_names=[cfg.OUTPUT_TENSOR])
    predictor = OfflinePredictor(pred_config)
    print("........Exporting Frozen File")
    ModelExporter(pred_config).export_compact(pb_path, optimize=False, toco_compatible=False)

    return predictor
 

def update_prototxt():
    print('........You can add the deconvolution layers')
    print('........There is the differance between tensorflow and caffe in deconvolution layer')
    print('........Produce caffe model and prototxt file')
    caffemodel_filepath = os.path.join('./out', 'tf2caffemodel.caffemodel')
    prototxt_filepath = os.path.join('./out', 'tf2caffemodel.prototxt')
    
    #net = caffe.Net(prototxt_filepath, caffemodel_filepath, caffe.TEST)
    net = caffe_pb2.NetParameter()
    with open(prototxt_filepath, 'r') as f:
        pb.text_format.Merge(f.read(), net)

    if net.layer[0].name != 'image':
        net.layer[0].name = 'image'
        net.layer[0].top[0] = 'image'
        net.layer[1].bottom[0] = 'image'
    
    final_blob_name = net.layer[len(net.layer)-1].name
    #####################################################################
    # need to add your additional layers...
    # the part which is different with caffe... 
    CLASS_NUM = 5

    deconv1 = net.layer.add()
    deconv1.CopyFrom(L.Deconvolution(convolution_param=dict(kernel_size=5, stride=2, num_output=64)).to_proto().layer[0])
    deconv1.name = 'deconv1_1'
    deconv1.top[0] = 'deconv1_1'
    deconv1.bottom.append('pool5_MaxPool')
    
    deconv2 = net.layer.add()
    deconv2.CopyFrom(L.Deconvolution(convolution_param=dict(kernel_size=5, stride=2, num_output=32)).to_proto().layer[0])
    deconv2.name = 'deconv2_1'
    deconv2.top[0] = 'deconv2_1'
    deconv2.bottom.append('deconv1_1')
    
    deconv3 = net.layer.add()
    deconv3.CopyFrom(L.Deconvolution(convolution_param=dict(kernel_size=5, stride=2, num_output=16)).to_proto().layer[0])
    deconv3.name = 'deconv3_1'
    deconv3.top[0] = 'deconv3_1'
    deconv3.bottom.append('deconv2_1')

    deconv4 = net.layer.add()
    deconv4.CopyFrom(L.Deconvolution(convolution_param=dict(kernel_size=5, stride=2, num_output=8)).to_proto().layer[0])
    deconv4.name = 'deconv4_1'
    deconv4.top[0] = 'deconv4_1'
    deconv4.bottom.append('deconv3_1')
    
    upscore = net.layer.add()
    upscore.CopyFrom(L.Deconvolution(convolution_param=dict(kernel_size=5, stride=2, num_output=CLASS_NUM)).to_proto().layer[0])
    upscore.name = 'upscore'
    upscore.top[0] = 'upscore'
    upscore.bottom.append('deconv4_1')
    
    with open(prototxt_filepath, 'w+') as f:
        f.write(pb.text_format.MessageToString(net))
    print(net)
    new_net = caffe.Net(prototxt_filepath, caffe.TEST)
    new_net.save('./temp.caffemodel')
    
    #net = caffe.Net(prototxt_filepath, './temp.caffemodel', caffe.TEST)

def update_caffemodel(weight_dir):
    prototxt_filepath = os.path.join('./out', 'tf2caffemodel.prototxt')
    caffemodel_filepath = './temp.caffemodel'
     
    net = caffe.Net(prototxt_filepath, caffemodel_filepath, caffe.TEST)
    
    for blob in net.blobs:
        print(blob)
        if 'deconv' in blob:
            weights = np.load(os.path.join(weight_dir, '{}_weights.npy'.format(blob)))
            #print('debug', net.params[blob][1].data[...].shape)
            net.params[blob][0].data[...] = weights
            if 'deconv4_1' in blob:
                print('Pass')
            else:
                print('biases')
                biases = np.load(os.path.join(weight_dir, '{}_biases.npy'.format(blob)))
                net.params[blob][1].data[...] = biases           
        elif 'conv' in blob:    
            weights = np.load(os.path.join(weight_dir, '{}_weights.npy'.format(blob)))
            biases  = np.load(os.path.join(weight_dir, '{}_biases.npy'.format(blob)))
            net.params[blob][0].data[...] = weights
            net.params[blob][1].data[...] = biases
        elif 'upscore' in blob:
            weights = np.load(os.path.join(weight_dir, '{}_weights.npy'.format(blob)))
            biases  = np.load(os.path.join(weight_dir, '{}_biases.npy'.format(blob)))
            net.params[blob][0].data[...] = weights
            net.params[blob][1].data[...] = biases
    
    result = net.forward()
    net.save('./out/tf2caffemodel.caffemodel')
    

def make_pb_file(input_filepath, model_path, pb_path, cfg, mode='bbox'):
    
    with tf.Session() as sess:
        input = cv2.imread(input_filepath, cv2.IMREAD_COLOR)

        print('........Resizing Image to ({}, {})'.format(cfg.IMAGE_HEIGHT, cfg.IMAGE_WIDTH))
        resize = cv2.resize(input, (cfg.IMAGE_WIDTH, cfg.IMAGE_HEIGHT), cv2.INTER_LINEAR)
        
        print('........Prediction')     
        predictor = predict(model_path, pb_path, cfg)
        result = predictor(np.expand_dims(resize, 0))
        
        if mode == 'segmentation':
            onehot_label = utils.onehot(np.squeeze(result[0].astype(np.uint8)), 4)
            label_rgb = utils.multihotcoding2img(onehot_label)
        
            return label_rgb
        else:
            from helpers.visualizer import Drawer
            keep = result[1] > 0.9
            result[2] = result[2][keep]
            result[1] = result[1][keep]
            result[0] = result[0][keep]
            
            drawer = Drawer(['background', 'vehicle', 'cyclist', 'person'], font_scale=0.5)
            resize = cv2.cvtColor(resize, cv2.COLOR_RGB2BGR)
            viz = drawer.bboxes_draw_on_img(resize[:, :, ::-1], result[2], result[0], result[1])            
            
            return viz
                    

def extract_npy_files_from_caffemodel(model_name, out_path):
    prototxt_filepath = os.path.join('./out', '{}.prototxt'.format(model_name))
    caffemodel_filepath = os.path.join('./out', '{}.caffemodel'.format(model_name))
    
    while True:
        if os.path.isfile(prototxt_filepath) and os.path.isfile(caffemodel_filepath):
            break

    net = caffe.Net(prototxt_filepath, caffemodel_filepath, caffe.TEST)   

    for blob in net.blobs:
        if 'conv' in blob:
            np.save(os.path.join(out_path, '{}_weights.npy'.format(blob)), net.params[blob][0].data[...])
            np.save(os.path.join(out_path, '{}_biases.npy'.format(blob)), net.params[blob][1].data[...])


def extract_npy_files_for_deconv(pb_path, out_path):
    print('........Extract the npy files from deconvolution')
    
    with tf.Session() as sess:
        with gfile.FastGFile(pb_path, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            sess.graph.as_default()
            tf.import_graph_def(graph_def, name='')
            graph_nodes=[n for n in graph_def.node]
            wts = [n for n in graph_nodes if n.op=='Const']
                  
            for n in wts:
                weights = tensor_util.MakeNdarray(n.attr['value'].tensor)
                if 'deconv' in n.name or 'upscore' in n.name:
                    if 'weights' in n.name:
                        weights = np.transpose(weights, (3, 2, 0, 1))
                        np.save(os.path.join(out_path, '{}.npy'.format(n.name.replace('/', '_'))), weights)
                    elif 'biases' in n.name:
                        np.save(os.path.join(out_path, '{}.npy'.format(n.name.replace('/', '_'))), weights)


if __name__ == '__main__':
    print(" #######################################################################")
    print(" * Bounding Box : Just do one step such as below,") 
    print("                  python tf2caffemodel.py ")
    print(" * Segmentation : Need one more step than Bounding box")
    print(" #######################################################################")
    parser = argparse.ArgumentParser()
    parser.add_argument('--step', default='simple')
    parser.add_argument('--mode', default='segmentation')
    parser.add_argument('--show', default=True)
    args = parser.parse_args()

    TEST_IMAGE = "/home/yeonhwa/DB/phantom_with_cityscape/phantom_test_set/image/image0.png"
    CFG_FILE   = "./config/segmentation.json"
    MODEL_PATH = "/home/yeonhwa/phantom-models/devbox03/renesas/"
    MODEL_NAME = "model-699392"
    PB_PATH    = "./out/" + MODEL_NAME + ".pb"
    OUT_SHAPE  = "288,512,3"
    OUT_NAME   = "pool5/MaxPool"

    # create folder for output files
    if not os.path.exists('./out'):
        os.makedirs('./out')
    
    weights_dir = os.path.join('./out', 'weights')
    if not os.path.isdir(weights_dir):
        os.makedirs(weights_dir)

    # Load Configuration
    cfg.load(CFG_FILE)
    
    if args.step == 'simple':
        # Make a frozen file
        result = make_pb_file(TEST_IMAGE, MODEL_PATH + MODEL_NAME, PB_PATH, cfg, mode=args.mode)    
        
        if args.show == True:
            cv2.imshow('DEBUG', result)
            cv2.waitKey(0)

        # Convert frozen file to caffemodel without Deconvolution
        r1, r2 = subprocess.Popen(['mmconvert', '-sf', 'tensorflow', '-iw', PB_PATH, '--inNodeName', cfg.INPUT_TENSOR, '--inputShape', OUT_SHAPE, '--dstNodeName', OUT_NAME, '-df', 'caffe', '-om', './out/tf2caffemodel'])
    else:
        # Convert frozen file to npy files for Deconvolution
        extract_npy_files_for_deconv(PB_PATH, weights_dir)
        extract_npy_files_from_caffemodel('tf2caffemodel', weights_dir)
    
        # make a new caffemodel 
        update_prototxt()
        update_caffemodel(weights_dir)
    
